// Copyright © 2019 by CommanderDonkey
// Read README.md for using

let speedo = mp.browsers.new("package://speedometer/index.html");
let showed = false;
let player = mp.players.local;

let gas = 0;

mp.events.add('getFuel', (player, args) => {

	gas = `${ args }` * 2;
})

mp.events.add('render', () =>
{
	if (player.vehicle && player.vehicle.getPedInSeat(-1) === player.handle)
	{
		if(showed === false)
		{
			speedo.execute("showSpeedo();");
			showed = true;
		}

		let vel1 = player.vehicle.getSpeed() * 2.236936;
        let vel = (vel1).toFixed(0);
		
		speedo.execute(`update(${vel}, ${gas});`);
		mp.game.graphics.notify(gas);

	}
	else
	{
		if(showed)
		{
			speedo.execute("hideSpeedo();");
			showed = false;
		}
	}
});