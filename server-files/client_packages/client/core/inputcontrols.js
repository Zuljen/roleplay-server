//const localPlayer = mp.players.local;
var test = false;

mp.events.add('checkChatOn', () => {
    test = true;
    inputOff();
});
mp.events.add('checkChatOff', () => {
    test = false;
    inputOn();
});

mp.events.add("render", () => {

    if (test == true) {
        mp.game.controls.disableAllControlActions(32)
    }
    if (test == false) {
        mp.game.controls.enableAllControlActions(32)
        mp.game.controls.enableAllControlActions(2)
    }

    if (mp.players.local.vehicle) {
        mp.game.audio.setRadioToStationName("OFF");
        mp.game.audio.setUserRadioControlEnabled(false);

        var height = mp.players.local.getHeightAboveGround();
        mp.game.graphics.notify(height);

    }

    mp.game.controls.disableControlAction(27, 60, true);
    mp.game.controls.disableControlAction(27, 61, true);
    mp.game.controls.disableControlAction(27, 62, true);
    mp.game.controls.disableControlAction(27, 63, true);
    mp.game.controls.disableControlAction(27, 64, true);
    mp.game.controls.disableControlAction(27, 89, true);
    mp.game.controls.disableControlAction(27, 90, true);






});

function inputOn() {

    mp.events.call('inputEnabled');
    mp.events.callRemote('EnableInputSS');
}

function inputOff() {

    mp.events.call('inputDisabled');
    mp.events.callRemote('DisableInputSS');
}