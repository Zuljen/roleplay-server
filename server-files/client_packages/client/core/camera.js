const updateInterval = 500;


mp.events.add("LOBBYCAM1", (player) => {

    mp.game.controls.disableAllControlActions(0);
    mp.game.controls.disableAllControlActions(1);

    let sceneryCamera = mp.cameras.new('default', new mp.Vector3(184.9754, -2283.17, 75.48703), new mp.Vector3(0, 0, 0), 40);

    sceneryCamera.pointAtCoord(-2768.318, -2543.384, 311.8793); //Changes the rotation of the camera to point towards a location
    sceneryCamera.setActive(true);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

mp.events.add("LOBBYCAM2", (player) => {

    mp.game.controls.disableAllControlActions(0);
    mp.game.controls.disableAllControlActions(1);
    let lobbyCam = mp.cameras.new('default', new mp.Vector3(184.9754, -2283.17, 75.48703), new mp.Vector3(0, 0, 0), 40);

    lobbyCam.pointAtCoord(-2768.318, -2543.384, 311.8793); //Changes the rotation of the camera to point towards a location
    lobbyCam.setActive(true);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

mp.events.add("ENDLOBBY", (player) => {

    mp.game.controls.disableAllControlActions(0);
    mp.game.controls.disableAllControlActions(1);

    mp.game.cam.renderScriptCams(false, false, 0, true, false);
});
