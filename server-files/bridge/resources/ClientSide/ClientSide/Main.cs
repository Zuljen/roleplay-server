﻿using System;
using RAGE;
using RAGE.Game;

namespace ClientSide
{
    public class Main : Events.Script
    {
        public Main()
        {
            Events.Add("FreezePlayerClient", FreezePlayer);
        }

        public void FreezePlayer(object [] args)
        {
            RAGE.Elements.Player.LocalPlayer.FreezePosition((bool)args[0]);
        }        
    }
}
