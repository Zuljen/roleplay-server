﻿using GTANetworkAPI;
using WiredPlayers.globals;
using WiredPlayers.character;
using WiredPlayers.model;
using WiredPlayers.database;
using WiredPlayers.messages.information;
using WiredPlayers.messages.error;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;

namespace WiredPlayers.jobs
{
    public class Mining : Script
    {
        public static List<PlayerModel> playerList;
        public static Dictionary<int, ItemModel> ItemCollection;
        public static Dictionary<int, Timer> miningTimerList;
        public List<Vector3> CopperCoords = new List<Vector3>();
        public List<Vector3> IronCoords = new List<Vector3>();
        public static List<OreModel> ore = new List<OreModel>();
        public OreModel CopperItem;
        public OreModel IronItem;
        public List<OreModel> allminingItems = new List<OreModel>();
        public Mining()
        {
            // Initialize the variables
            miningTimerList = new Dictionary<int, Timer>();
            NAPI.World.DeleteWorldProp(-18398025, new Vector3(-596.30225, 2088.9402, 131.41283), (15.0f));
            NAPI.World.DeleteWorldProp(-872784146, new Vector3(-596.30225, 2088.9402, 131.41283), (15.0f));
            NAPI.World.DeleteWorldProp(-1241212535, new Vector3(-596.30225, 2088.9402, 131.41283), (15.0f));
            GenerateCopperCoords();
            GenerateIronCoords();
        }
        public void GenerateCopperCoords()
        {
            CopperCoords.Add(new Vector3(-593.95197, 2078.6904, 130.57394));
            CopperCoords.Add(new Vector3(-591.6027, 2075.5366, 130.48064));
            CopperCoords.Add(new Vector3(-590.6442, 2064.8406, 130.20831));
            CopperCoords.Add(new Vector3(-587.8685, 2060.606, 129.75726));
            CopperCoords.Add(new Vector3(-589.2712, 2052.7168, 129.17943));
            CopperCoords.Add(new Vector3(-585.1507, 2046.242, 129.5875));
            CopperCoords.Add(new Vector3(-584.5333, 2038.907, 129.145));
            CopperCoords.Add(new Vector3(-577.2247, 2032.859, 128.3855));
            CopperCoords.Add(new Vector3(-575.8845, 2026.687, 128.1181));
            CopperCoords.Add(new Vector3(-567.9684, 2021.757, 127.6449));
            CopperCoords.Add(new Vector3(-564.9797, 2015.08, 127.3812));
            CopperCoords.Add(new Vector3(-559.0182, 2007.495, 127.1938));
            CopperCoords.Add(new Vector3(-550.9442, 1998.582, 127.0662));
            CopperCoords.Add(new Vector3(-549.7714, 1992.081, 127.0293));
            CopperCoords.Add(new Vector3(-544.5245, 1988.185, 127.0208));
            CopperCoords.Add(new Vector3(-546.1243, 1983.032, 127.0993));

            GenerateOre();
        }
        public void GenerateIronCoords()
        {
            IronCoords.Add(new Vector3(-536.5048, 1980.177, 127.1066));
            IronCoords.Add(new Vector3(-527.9396, 1981.272, 126.8919));
            IronCoords.Add(new Vector3(-519.8535, 1977.967, 126.5759));
            IronCoords.Add(new Vector3(-511.0958, 1977.526, 126.4923));
            IronCoords.Add(new Vector3(-502.8426, 1980.972, 125.9305));
            IronCoords.Add(new Vector3(-481.4002, 1985.859, 124.2487));
            IronCoords.Add(new Vector3(-492.7108, 1981.635, 125.0135));
            GenerateOre();
        }
        public void GenerateOre()
        {
            foreach (Vector3 o in CopperCoords)
            {
                GTANetworkAPI.Object Copper = NAPI.Object.CreateObject(1471437843, o, new Vector3(0, 0, 0), 255, 0);
                Vector3 CopperPos = new Vector3(0, 0, 0);
                CopperPos = new Vector3(o.X, o.Y, o.Z + 0.5f);
                OreModel CopperItem = new OreModel();
                CopperItem.itemPosition = CopperPos;
                CopperItem.itemName = "Copper";
                CopperItem.itemDesc = "Copper Metal can be sold to a factory.";
                CopperItem.itemObject = Copper;
                CopperItem.itemHud = NAPI.TextLabel.CreateTextLabel("Copper", CopperPos, 10, 2, 4, new Color(255, 255, 255), true, 0);
                allminingItems.Add(CopperItem);
            }
            foreach (Vector3 o in IronCoords)
            {
                GTANetworkAPI.Object Iron = NAPI.Object.CreateObject(1471437843, o, new Vector3(0, 0, 0), 255, 0);
                Vector3 newPos = new Vector3(0, 0, 0);
                newPos = new Vector3(o.X, o.Y, o.Z + 0.5f);
                OreModel IronItem = new OreModel();
                IronItem.itemPosition = newPos;
                IronItem.itemName = "Iron";
                IronItem.itemDesc = "Iron Ingots can be sold to a factory.";
                IronItem.itemObject = Iron;
                IronItem.itemHud = NAPI.TextLabel.CreateTextLabel("Iron", newPos, 10, 2, 4, new Color(255, 255, 255), true, 0);
                allminingItems.Add(IronItem);
            }

        }
        public static void OnPlayerDisconnected(Client player)
        {
            if (miningTimerList.TryGetValue(player.Value, out Timer miningTimer))
            {
                // Remove the timer
                miningTimer.Dispose();
                miningTimerList.Remove(player.Value);
            }
        }
        public static void AnimateMining(Client player, bool isMining)
        {
            // Play the animation
            player.PlayAnimation("amb@world_human_gardener_plant@male@idle_a", "idle_a", (int)(Constants.AnimationFlags.AllowPlayerControl | Constants.AnimationFlags.Loop));

            // Create the timer
            Timer miningTimer = null;

            if (isMining)
            {
                // Create the mining timer
                miningTimer = new Timer(CollectOreAsync, player, 5000, Timeout.Infinite);
            }
            // Add the timer to the list
            miningTimerList.Add(player.Value, miningTimer);
        }
        public static void AnimateMiningIron(Client player, bool isMining)
        {
            // Play the animation
            player.PlayAnimation("amb@world_human_gardener_plant@male@idle_a", "idle_a", (int)(Constants.AnimationFlags.AllowPlayerControl | Constants.AnimationFlags.Loop));

            // Create the timer
            Timer miningTimer = null;

            if (isMining)
            {
                // Create the mining timer
                miningTimer = new Timer(CollectIronAsync, player, 5000, Timeout.Infinite);
            }
            // Add the timer to the list
            miningTimerList.Add(player.Value, miningTimer);
        }
        public static PlayerModel GetCharacterById(int characterId)
        {
            // Get the business given an specific identifier
            return playerList.Where(character => character.id == characterId).FirstOrDefault();
        }


       // [RemoteEvent("miningSuccess")]
        /*   public async Task CompletedOreRemoteEventSync(Client player)
           {
               if (miningTimerList.TryGetValue(player.Value, out Timer miningTimer))
               {
                   miningTimerList.Remove(player.Value);
                   miningTimer.Dispose();
                   player.StopAnimation();
                   player.ResetData(EntityData.PlayerMining);

                   int playerDatabaseId = player.GetData(EntityData.PLAYER_SQL_ID);
                   ItemModel CopperItem = Globals.GetPlayerItemModelFromHash(playerDatabaseId, Constants.ITEM_HASH_COPPER);

                   if (CopperItem == null)
                   {
                       CopperItem = new ItemModel()
                       {

                           amount = 1,
                           hash = Constants.ITEM_HASH_COPPER,
                           ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                           ownerIdentifier = playerDatabaseId,
                           position = new Vector3(),
                           dimension = 0
                       };

                       // Add the fish item to database
                       CopperItem.id = Database.AddNewItem(CopperItem);
                       Inventory.ItemCollection.Add(CopperItem.id, CopperItem);
                   }
                   else
                   {
                       // Update the inventory
                       CopperItem.amount += 1;
                       await Task.Run(() => Database.UpdateItem(CopperItem)).ConfigureAwait(false);

                   }

                   // Send the message to the player
                   player.SendChatMessage(Constants.COLOR_INFO + "You mined 1 Copper");
               }
           }*/
        [Command("mining")]
        [RemoteEvent("actionkeyE")]
        public void MiningCommand(Client player)
        {
            foreach (OreModel CopperItem in allminingItems)
            {
                if (CopperItem.itemPosition.DistanceTo(player.Position) < 5)
                {
                    if (miningTimerList.ContainsKey(player.Value))
                    {
                        player.SendChatMessage(Constants.COLOR_ERROR + "You are already mining.");
                        return;
                    }
                    if (CopperItem.itemFound = false)
                        return;

                    CopperItem.itemFound = true;
                    NAPI.Util.ConsoleOutput("We made it here");
                    CopperItem.itemHud.Delete();
                    NAPI.Util.ConsoleOutput("We made it here aswell");
                    CopperItem.itemObject.Delete();
                    NAPI.Util.ConsoleOutput("We made it here3");
                    if (miningTimerList.ContainsKey(player.Value))
                    {
                        player.SendChatMessage(Constants.COLOR_ERROR + "You are already mining.");
                        return;
                    }

                   if (player == player)
                    {
                        //  string rightHand = player.GetSharedData(EntityData.PLAYER_RIGHT_HAND);
                        //  int pickeaxeId = NAPI.Util.FromJson<AttachmentModel>(rightHand).itemId;
                        //  ItemModel pickaxe = Inventory.GetPlayerItemModelFromHash(Constants.ITEM_HASH_PICKAXE);

                        //  if (pickaxe == null || pickaxe.hash != Constants.ITEM_HASH_PICKAXE)
                        //  {
                        //      player.SendChatMessage(Constants.COLOR_ERROR + "You do not have a pickaxe equipped.");
                        //      return;
                        //  }

                        //int playerId = player.GetExternalData<CharacterModel>((int)ExternalDataSlot.Database).Id;

                        player.SetData(EntityData.PlayerMining, true);
                        player.PlayAnimation("amb@world_human_stand_fishing@base", "base", (int)Constants.AnimationFlags.Loop);
                        AnimateMining(player, true);
                        player.TriggerEvent("playermining"); // Not required atm
                        return;

                    }

                }
            }
          /*  foreach (OreModel IronItem in allminingItems)
            {
                {
                    if (IronItem.itemFound = false)
                        return;

                    IronItem.itemFound = true;
                    NAPI.Util.ConsoleOutput("We made it here too");
                    IronItem.itemHud.Delete();
                    IronItem.itemObject.Delete();
                    if (miningTimerList.ContainsKey(player.Value))
                    {
                        player.SendChatMessage(Constants.COLOR_ERROR + "You are already mining.");
                        return;
                    }

                    if (player == player)
                    {
                        //  string rightHand = player.GetSharedData(EntityData.PLAYER_RIGHT_HAND);
                        //  int pickeaxeId = NAPI.Util.FromJson<AttachmentModel>(rightHand).itemId;
                        //  ItemModel pickaxe = Inventory.GetPlayerItemModelFromHash(Constants.ITEM_HASH_PICKAXE);

                        //  if (pickaxe == null || pickaxe.hash != Constants.ITEM_HASH_PICKAXE)
                        //  {
                        //      player.SendChatMessage(Constants.COLOR_ERROR + "You do not have a pickaxe equipped.");
                        //      return;
                        //  }

                        //int playerId = player.GetExternalData<CharacterModel>((int)ExternalDataSlot.Database).Id;

                        player.SetData(EntityData.PlayerMining, true);
                        player.PlayAnimation("amb@world_human_stand_fishing@base", "base", (int)Constants.AnimationFlags.Loop);
                        AnimateMiningIron(player, true);
                        player.TriggerEvent("startMining"); // Not required atm
                        return;

                    }

                }
            }*/
           
        }
    

        private static void CollectOreAsync(object oreItem)
        {
            // Get the client mining
            Client player = (Client)oreItem;

            // Stop the mining animation
            player.StopAnimation();

            // Give the ore to the player
            int amount = 1;

            // Check if the player has any Copper ore in the inventory
            int playerId = player.GetData(EntityData.PLAYER_SQL_ID);
            ItemModel CopperItem = Globals.GetPlayerItemModelFromHash(playerId, Constants.ITEM_HASH_COPPER);

            if (CopperItem == null)
            {
                // Create the object
                CopperItem = new ItemModel()
                {
                    amount = amount,
                    dimension = 0,
                    position = new Vector3(),
                    hash = Constants.ITEM_HASH_COPPER,
                    ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                    ownerIdentifier = playerId,
                    objectHandle = null
                };
                Database.AddNewItem(CopperItem);
                //    Inventory.ItemCollection.Add(CopperItem.id, CopperItem);
            }
            else
            {
                // Add the amount
                CopperItem.amount += amount;

                // Update the amount into the database
                // Update the item's amount
                player.SetData(EntityData.PlayerMining, false);
                Database.UpdateItem(CopperItem);
              //  Globals.itemList.Add(CopperItem);
                // Send the confirmation message
                player.SendNotification("You gained 1 XP in Mining");
                player.SendNotification("You gained 1 XP in Strenght");
                player.SendChatMessage(Constants.COLOR_YELLOW + "You mined 1 Copper Ore");
            }


            // Remove the timer from the list
            miningTimerList.Remove(player.Value);

            List<SkillsModel> PlayerSkillList = Database.LoadSkills();

            foreach (SkillsModel skills in PlayerSkillList)
            {
                if (skills.id == player.GetData(EntityData.PLAYER_SQL_ID))
                {
                    skills.miningexp = skills.miningexp + 1;
                    skills.strenghtexp = skills.strenghtexp + 1;

                    Database.SaveSkills(PlayerSkillList);
                }
            }
            /*            // Check if the player has any Ìron ore in the inventory
            ItemModel IronItem = Globals.GetPlayerItemModelFromHash(playerId, Constants.ITEM_HASH_IRON);
            if (IronItem == null)
            {
                // Create the object
                IronItem = new ItemModel()
                {
                    amount = amount,
                    dimension = 0,
                    position = new Vector3(),
                    hash = Constants.ITEM_HASH_IRON,
                    ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                    ownerIdentifier = playerId,
                    objectHandle = null
                };

                Database.AddNewItem(IronItem);
                //    Inventory.ItemCollection.Add(IronItem.id, IronItem);
            }
            else
            {
                // Add the amount
                IronItem.amount += amount;

                // Update the amount into the database
                // Update the item's amount
                player.SetData(EntityData.PlayerMining, false);
                Database.UpdateItem(IronItem);
                Globals.itemList.Add(IronItem);
                player.SendChatMessage(Constants.COLOR_YELLOW + "You mined 1 Iron Ore");


            }*/
        }
        private static void CollectIronAsync(object oreItem)
        {
            // Get the client mining
            Client player = (Client)oreItem;

            // Stop the mining animation
            player.StopAnimation();

            // Give the ore to the player
            int amount = 1;

            // Check if the player has any Copper ore in the inventory
            int playerId = player.GetData(EntityData.PLAYER_SQL_ID);
            ItemModel CopperItem = Globals.GetPlayerItemModelFromHash(playerId, Constants.ITEM_HASH_COPPER);

            if (CopperItem == null)
            {
                // Create the object
                CopperItem = new ItemModel()
                {
                    amount = amount,
                    dimension = 0,
                    position = new Vector3(),
                    hash = Constants.ITEM_HASH_COPPER,
                    ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                    ownerIdentifier = playerId,
                    objectHandle = null
                };
                Database.AddNewItem(CopperItem);
                //    Inventory.ItemCollection.Add(CopperItem.id, CopperItem);
            }
            else
            {
                // Add the amount
                CopperItem.amount += amount;

                // Update the amount into the database
                // Update the item's amount
                player.SetData(EntityData.PlayerMining, false);
                Database.UpdateItem(CopperItem);
                Globals.itemList.Add(CopperItem);
                // Send the confirmation message
                player.SendNotification("You gained 1 XP in Mining");
                player.SendNotification("You gained 1 XP in Strenght");
                player.SendChatMessage(Constants.COLOR_YELLOW + "You mined 1 Copper Ore");
            }


            // Remove the timer from the list
            miningTimerList.Remove(player.Value);

            List<SkillsModel> PlayerSkillList = Database.LoadSkills();

            foreach (SkillsModel skills in PlayerSkillList)
            {
                if (skills.id == player.GetData(EntityData.PLAYER_SQL_ID))
                {
                    skills.miningexp = skills.miningexp + 1;

                    Database.SaveSkills(PlayerSkillList);
                }
            }
            
        }
    }
}



