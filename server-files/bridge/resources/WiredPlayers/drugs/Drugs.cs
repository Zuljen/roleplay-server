using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WiredPlayers.character;
using WiredPlayers.model;
using WiredPlayers.messages.help;
using WiredPlayers.messages.information;
using WiredPlayers.globals;
using WiredPlayers.database;
using WiredPlayers.messages.error;
using WiredPlayers.jobs;

namespace WiredPlayers.drugs
{

    class Drugs : Script
    {
        public static List<Vector3> Allleafs = new List<Vector3>();
        public static Dictionary<int, Timer> harvestTimerList;
        public static Dictionary<int, Timer> PlantingTimer;
        public List<PlantModel> allCocainItems = new List<PlantModel>();

        public Drugs()
        {
            harvestTimerList = new Dictionary<int, Timer>();
            PlantingTimer = new Dictionary<int, Timer>();
            GenerateCocainCoords();
          //  Database.LoadAllPlants();
        }


        [Command(Commands.COM_REGENLEAFS)]
        public void RegenleafsCommand(Client player)
        {
            GenerateLeafs();
        }

        public void GenerateCocainCoords()
        {
            Allleafs.Add(new Vector3(2937.354, 5336.313, 102.0025));
            Allleafs.Add(new Vector3(2953.295, 5342.489, 102.6044));
            Allleafs.Add(new Vector3(2971.354, 5330.149, 100.6253));
            Allleafs.Add(new Vector3(2994.591, 5345.255, 98.37457));
            Allleafs.Add(new Vector3(1368.468, -1039.106, 43.80099));
            Allleafs.Add(new Vector3(1455.644, -566.085, 85.41368));
            Allleafs.Add(new Vector3(1470.736, -547.3842, 86.49038));
            Allleafs.Add(new Vector3(1576.542, -595.4244, 148.702));
            Allleafs.Add(new Vector3(2682.375, -861.5181, 26.05725));
            Allleafs.Add(new Vector3(2679.046, -855.9504, 26.87387));
            Allleafs.Add(new Vector3(2690.958, -847.2991, 26.8414));
            Allleafs.Add(new Vector3(2662.715, -859.8538, 27.61178));
            Allleafs.Add(new Vector3(2628.808, 4753.696, 33.94017));
            Allleafs.Add(new Vector3(-359.7701, 4313.289, 57.04502));
            Allleafs.Add(new Vector3(-2178.192, -37.99974, 70.90199));
            Allleafs.Add(new Vector3(-1986.918, 2510.394, 2.961013));
            Allleafs.Add(new Vector3(-1114.256, 4959.53, 218.9207));
            GenerateLeafs();
        }
        public void GenerateLeafs()
        {
            foreach (Vector3 o in Allleafs)
            {
                GTANetworkAPI.Object Leaf = NAPI.Object.CreateObject(2575791413, o, new Vector3(0, 0, -0.3), 255, 0);
                Vector3 LeafPos = new Vector3(0, 0, 0);
                LeafPos = new Vector3(o.X, o.Y, o.Z + 0.5f);
                PlantModel LeafItem = new PlantModel();
                LeafItem.Position = LeafPos;
                LeafItem.itemName = "Cocain Leaf";
                LeafItem.itemDesc = "Used to create Cocain";
                LeafItem.itemObject = Leaf;
                LeafItem.itemHud = NAPI.TextLabel.CreateTextLabel("Cocain Leafs", LeafPos, 10, 2, 4, new Color(255, 255, 255), true, 0);
                LeafItem.itemHud2 = NAPI.TextLabel.CreateTextLabel("Press E to Collect", LeafPos, 10, 2, 4, new Color(255, 255, 255), true, 0);
                allCocainItems.Add(LeafItem);
            }
        }
        public static void OnPlayerDisconnected(Client player)
        {
            if (harvestTimerList.TryGetValue(player.Value, out Timer miningTimer))
            {
                // Remove the timer
                miningTimer.Dispose();
                harvestTimerList.Remove(player.Value);
                if (PlantingTimer.TryGetValue(player.Value, out Timer plantTimer))
                {
                    plantTimer.Dispose();
                    PlantingTimer.Remove(player.Value);
                }
            }
        }
        public static void AnimateHarvest(Client player, bool isHarvesting)
        {
            // Play the animation
            player.PlayAnimation("amb@world_human_gardener_plant@male@idle_a", "idle_a", (int)(Constants.AnimationFlags.AllowPlayerControl | Constants.AnimationFlags.Loop));

            // Create the timer
            Timer miningTimer = null;

            if (isHarvesting)
            {
                // Create the mining timer
                miningTimer = new Timer(CollectLeafAsync, player, 5000, Timeout.Infinite);
            }
            // Add the timer to the list
            harvestTimerList.Add(player.Value, miningTimer);
        }

        [Command("harvest")]
        [RemoteEvent("actionkeyE")]             
        public void HarvestCommand(Client player)

        {
            foreach (PlantModel LeafItem in allCocainItems)
            {
                if (LeafItem.Position.DistanceTo(player.Position) < 5)
                {
                    if (harvestTimerList.ContainsKey(player.Value))
                    {
                        player.SendChatMessage(Constants.COLOR_ERROR + "You are already mining.");
                        return;
                    }
                    if (LeafItem.itemFound == false)
                        return;

                    LeafItem.itemFound = true;
                    LeafItem.itemHud.Delete();
                    LeafItem.itemHud2.Delete();
                    LeafItem.itemObject.Delete();

                    if (player == player)
                    {
                        player.SetData(EntityData.PlayerMining, true);
                        player.PlayAnimation("amb@world_human_stand_fishing@base", "base", (int)Constants.AnimationFlags.Loop);
                        AnimateHarvest(player, true);
                        player.TriggerEvent("playermining"); // Not required atm
                        return;

                    }

                }
            }
        }

        private static void CollectLeafAsync(object Leaf)
        {
            // Get the client mining
            Client player = (Client)Leaf;

            // Stop the mining animation
            player.StopAnimation();

            // Give the ore to the player
            int amount = 1;

            // Check if the player has any Copper ore in the inventory
            int playerId = player.GetData(EntityData.PLAYER_SQL_ID);
            ItemModel LeafItem = Globals.GetPlayerItemModelFromHash(playerId, Constants.ITEM_HASH_LEAF);

            if (LeafItem == null)
            {
                // Create the object
                LeafItem = new ItemModel()
                {
                    amount = amount,
                    dimension = 0,
                    position = new Vector3(),
                    hash = Constants.ITEM_HASH_LEAF,
                    ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                    ownerIdentifier = playerId,
                    objectHandle = null
                };
                Database.AddNewItem(LeafItem);
                Inventory.ItemCollection.Add(LeafItem.id, LeafItem);
            }
            else
            {
                // Add the amount
                LeafItem.amount += amount;

                // Update the amount into the database
                // Update the item's amount
                player.SetData(EntityData.PlayerMining, false);
                Database.UpdateItem(LeafItem);
                //  Globals.itemList.Add(CopperItem);
                // Send the confirmation message
                player.SendNotification("You gained 1 XP in Herbalism");
                player.SendChatMessage(Constants.COLOR_YELLOW + "You harvested 1 Leafs");
            }


            // Remove the timer from the list
            harvestTimerList.Remove(player.Value);

            List<SkillsModel> PlayerSkillList = Database.LoadSkills();

            foreach (SkillsModel skills in PlayerSkillList)
            {
                if (skills.id == player.GetData(EntityData.PLAYER_SQL_ID))
                {
                    skills.miningexp = skills.miningexp + 1;

                    Database.SaveSkills(PlayerSkillList);
                }
            }
        }



        #region Weed Related
        public static List<PlantModel> Plants;
        private const int PlantGrowthTime = 120;
        private const int MaxWeedPerPlant = 10;
      //  player.SetData(EntityData.TIME_HOSPITAL_RESPAWN, Globals.GetTotalSeconds() + 600);
        [Command(Commands.COM_WEED, GreedyArg = true)]
        public static void PlantCommand(Client player)
        {
            /*  if (Emergency.IsPlayerDead(player))
              {
                  player.SendChatMessage(Constants.COLOR_ERROR + ErrRes.player_is_dead);
                  return;
              }
            */
            if (player.Dimension != 0)
            {
                player.SendChatMessage(Constants.COLOR_ERROR + ErrRes.cant_plant_indoor);
                return;
            }

            if (Drugs.PlantingTimer.ContainsKey(player.Value))
            {
                player.SendChatMessage(Constants.COLOR_ERROR + ErrRes.player_already_planting);
                return;
            }
            // Plant the seeds
            Drugs.AnimatePlayerWeedManagement(player, true);
            // Check the closest plant
            /*  PlantModel plant = GetClosestPlant(player);

              if (plant != null)
              {
                  player.SendChatMessage(Constants.COLOR_ERROR + ErrRes.player_has_plants_close);
                  return;
              }


                  // Plant the seeds
                  Drugs.AnimatePlayerWeedManagement(player, true);*/
        }
        public enum ColShapeTypes
        {
            BusinessEntrance = 0, BusinessPurchase = 1, HouseEntrance = 2,
            InteriorEntrance = 3, VehicleDealer = 5, Atm = 6, Plant = 7, Ore = 8
        }




        public static void UpdateGrowth()
        {
            foreach (PlantModel plant in Plants)
            {
                // Check if the plant has fully grown
                if (plant.GrowTime == PlantGrowthTime) continue;

                // Update the plant
                UpdatePlant(plant);
            }
        }

        public static void UpdatePlant(PlantModel plant)
        {
            // Get the growth percentage
            int growthTime = plant.Object == null ? plant.GrowTime : plant.GrowTime + 1;
            float growth = (float)Math.Round((decimal)(growthTime * 100 / PlantGrowthTime), 2);
            string growthText = string.Format(InfoRes.growth, Math.Floor(growth));

            // Create the corresponding object
            uint model = GetPlantModel(growth);

            if (plant.Object == null)
            {
                NAPI.Task.Run(() =>
                {
                    // Create the plant
                    plant.Object = NAPI.Object.CreateObject(model, plant.Position, new Vector3(), 255, plant.Dimension);

                    // Create the growth label
                    plant.Progress = NAPI.TextLabel.CreateTextLabel(growthText, new Vector3(0.0f, 0.0f, 4.0f).Add(plant.Position), 12.5f, 0.5f, 4, new Color(225, 200, 165), true, plant.Dimension);
                });
            }
            else
            {
                if (plant.Object.Model != model)
                {
                    NAPI.Task.Run(() =>
                    {
                        // Destroy the current object
                        plant.Object.Delete();

                        // Create the plant
                        plant.Object = NAPI.Object.CreateObject(model, plant.Position, new Vector3(), 255, plant.Dimension);
                    });
                }

                // Update the growth label
                plant.Progress.Text = growthText;
                plant.GrowTime++;

                // Update the plant's growth
                Database.ModifyPlant(plant);
            }

            if (plant.GrowTime == PlantGrowthTime)
            {
                NAPI.Task.Run(() =>
                {
                    // Create the colshape for the plant
                    plant.PlantColshape = NAPI.ColShape.CreateCylinderColShape(plant.Position, 3.5f, 1.0f, plant.Dimension);
                    plant.PlantColshape.SetData(EntityData.ColShapeId, plant.Id);

                    // Add the message to pop the instructional button up
                    // plant.PlantColshape.SetData(EntityData.ColShapeType, ColShapeTypes.Plant);
                    //  plant.PlantColshape.SetData(EntityData.InstructionalButton, HelpRes.action_plant);
                });
            }
        }

        /* public static PlantModel GetClosestPlant(Client player)
         {

             // Get the closest plant to the player
            // return Plants.FirstOrDefault(plant => plant.Dimension == player.Dimension && player.Position.DistanceTo(plant.Position) < 2.0f);
             return Plants.FirstOrDefault(plant => player.Position.DistanceTo(plant.Position) < 2.0f);
         }*/

        public static void AnimatePlayerWeedManagement(Client player, bool isPlanting)
        {
            // Play the animation
            player.PlayAnimation("amb@world_human_gardener_plant@male@idle_a", "idle_a", (int)(Constants.AnimationFlags.AllowPlayerControl | Constants.AnimationFlags.Loop));

            // Create the timer
            Timer plantManagementTimer = null;

            if (isPlanting)
            {
                // Create the plant timer
                plantManagementTimer = new Timer(PlantWeedSeedsAsync, player, 5000, Timeout.Infinite);
            }
            else
            {
                // Create the collect timer
                plantManagementTimer = new Timer(CollectWeedAsync, player, 5000, Timeout.Infinite);
            }

            // Add the timer to the list
            PlantingTimer.Add(player.Value, plantManagementTimer);
        }

        public static async void PlantWeedSeedsAsync(object planter)
        {
            // Get the client planting
            Client player = (Client)planter;

            // Stop the plant animation
            player.StopAnimation();

            // Create the new plant object
            PlantModel plant = new PlantModel();
            {
                plant.Position = new Vector3(player.Position.X, player.Position.Y, player.Position.Z - 1.0f);
                plant.Dimension = player.Dimension;
                plant.GrowTime = 0;
            }

            // Add the plant to the database
            // plant.Id = await DatabaseOperations.AddPlant(plant).ConfigureAwait(false);
            Database.AddPlant(plant);

            // Remove the timer from the list
            PlantingTimer.Remove(player.Value);

            // Create the ingame object
                Task.Run(() => UpdatePlant(plant)).ConfigureAwait(false);
        }
        public static async void CollectWeedAsync(object planter)
        {
            // Get the client planting
            Client player = (Client)planter;


            // Stop the plant animation
            player.StopAnimation();

            // Give the weed to the player
            Random random = new Random();
            int amount = random.Next(1, MaxWeedPerPlant);

            // Check if the player has any weed plant in the inventory
            int playerId = player.GetData(EntityData.PLAYER_SQL_ID);
            ItemModel weedItem = Inventory.GetPlayerItemModelFromHash(playerId, Constants.ITEM_HASH_WEED);

            if (weedItem == null)
            {
                //Create the object
                weedItem = new ItemModel()
                {
                    amount = amount,
                    dimension = 0,
                    position = new Vector3(),
                    hash = Constants.ITEM_HASH_WEED,
                    ownerEntity = Constants.ITEM_ENTITY_PLAYER,
                    ownerIdentifier = playerId,
                    objectHandle = null
                };

                Database.AddNewItem(weedItem);
                //   Inventory.ItemCollection.Add(weedItem.id, weedItem);
            }
            else
            {
                // Add the amount
                weedItem.amount += amount;

                // Update the amount into the database
                Database.AddNewItem(weedItem);
        }
        }

        // Get the closest plant
        /* PlantModel plant = GetClosestPlant(player);

            await Task.Run(() =>
            {
                NAPI.Task.Run(() =>
                {
                    // Remove the plant with its description
                    plant.PlantColshape.Delete();
                    plant.Progress.Delete();
                    plant.Object.Delete();
                });

                // Delete the row into the database
             //   DatabaseOperations.DeleteSingleRow("plants", "id", plant.Id);
             //   Plants.Remove(plant);
            }).ConfigureAwait(false);

            // Remove the timer from the list
            PlantingTimer.Remove(player.Value);

            // Send the confirmation message
            player.SendChatMessage(Constants.COLOR_INFO + string.Format(InfoRes.plant_collected, amount));*/
    

            
            private static uint GetPlantModel(float growth)
            {
                string modelName;

                if (growth < 25.0f) modelName = "bkr_prop_weed_plantpot_stack_01b";
                else if (growth < 50.0f) modelName = "bkr_prop_weed_01_small_01b";
                else if (growth < 75.0f) modelName = "bkr_prop_weed_med_01b";
                else modelName = "bkr_prop_weed_lrg_01b";
            //bkr_prop_weed_plantpot_stack_01b
            return NAPI.Util.GetHashKey(modelName);
            }
        }
    #endregion
}



