﻿using System;
using GTANetworkAPI;

namespace RP_TEST_GTA
{
    public class Main : Script
    {
        public void Log_Server(string message)
        {
            NAPI.Util.ConsoleOutput($"[LOG] {message}");

            return;
        }


        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            NAPI.Server.SetGlobalServerChat(false);
            Log_Server("Main Server CHECKED");
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client Player)
        {
            NAPI.Util.ConsoleOutput($"{Player.Name} has joinedddddd");
            NAPI.Server.SetGlobalServerChat(false);
        }

        [ServerEvent(Event.ChatMessage)]
        public void OnPlayerChat(Client player, string message)
        {
            /*
            NAPI.Util.ConsoleOutput($"[SERVER LOG] {player.Name} says: {message}");          
            
            var nearestPlayers = NAPI.Player.GetPlayersInRadiusOfPlayer(5.0, player);

            foreach (Client client in nearestPlayers)
            {
                client.SendChatMessage($"{player.Name} says: {message}");
            }
            */
        }
    }
}
