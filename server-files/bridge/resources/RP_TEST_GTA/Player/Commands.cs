﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;
using GTANetworkInternals;
using GTANetworkMethods;

namespace RP_TEST_GTA.Player
{
    class Commands : Script
    {
        #region Player Messaging Commands (IC)

        [Command("whisper", Alias = "w", GreedyArg = true)]
        public void CMD_Whisper(Client player, Client target, string message)
        {
            if (target.Equals(player))
            {
                // player.SendChatMessage("You can't whisper to yourself.");
                //return;
            }

            target.SendChatMessage($"~y~{player.Name} whispers~w~: {message}");
            player.SendChatMessage($"~y~ Whisper to {target.Name}~w~: {message}");
        }

        [Command("checkfreq")]
        public void CMD_CheckRadioFreq(Client player)
        {
            if (!NAPI.Data.HasEntitySharedData(player.Handle, "radiofreq"))
            {
                int freq = 0;
                NAPI.Data.SetEntitySharedData(player.Handle, "radiofreq", freq);
            }

            int freqCurrent = NAPI.Data.GetEntitySharedData(player.Handle, "radiofreq");

            if (freqCurrent == 0)
            {
                player.SendChatMessage("You haven't set the frequency of your radio.");
            }
            else
            {
                player.SendChatMessage("Your frequency is set to " + freqCurrent); 
            }
        }

        [Command("radiofreq")]
        public void CMD_RadioFreq(Client player, int freq)
        {
            if (freq > 5000 || freq < 1)
            {
                player.SendChatMessage($"The frequency must between 1 and 5000.");
                return;
            }
           
            NAPI.Data.SetEntitySharedData(player.Handle, "radiofreq", freq);
            player.SendChatMessage($"You set the Radio Frequency to: {freq}");
        }

        [Command("radio", Alias = "r", GreedyArg = true)]
        public void CMD_Radio(Client player, string message)
        {
            if (!NAPI.Data.HasEntitySharedData(player.Handle, "radiofreq"))
            {
                int freq = 0;
                NAPI.Data.SetEntitySharedData(player.Handle, "radiofreq", freq);
            }

            int radiofreq = NAPI.Data.GetEntitySharedData(player.Handle, "radiofreq");

            if (radiofreq == 0)
            {
                player.SendChatMessage("You haven't set the radio frequency.");
                return;
            }

            var nearestPlayers = NAPI.Player.GetPlayersInRadiusOfPlayer(5.0, player);
            var allPlayers = NAPI.Pools.GetAllPlayers();

            foreach (Client client in allPlayers)
            {
                if (!NAPI.Data.HasEntitySharedData(client.Handle, "radiofreq"))
                {
                    int freq = 0;
                    NAPI.Data.SetEntitySharedData(player.Handle, "radiofreq", freq);
                }
                int clientRadioFreq = NAPI.Data.GetEntitySharedData(client.Handle, "radiofreq");

                if (clientRadioFreq == radiofreq)
                {
                    client.SendChatMessage($"~o~[Radio] ~w~{player.Name}: {message}");
                }
            }

            foreach (Client client in nearestPlayers)
            {
                client.SendChatMessage($"[Radio] {player.Name}: {message}");
            }
        }

        [Command("shout", Alias = "s", GreedyArg = true)]
        public void CMD_Shout(Client player, string message)
        {
            NAPI.Util.ConsoleOutput($"[SERVER LOG] {player.Name} shouts: {message}");

            var nearestPlayers = NAPI.Player.GetPlayersInRadiusOfPlayer(15.0, player);

            foreach (Client client in nearestPlayers)
            {
                client.SendChatMessage($"{player.Name} shouts: {message}");
            }
        }

        [Command("low", Alias = "l", GreedyArg = true)]
        public void CMD_Low(Client player, string message)
        {
            NAPI.Util.ConsoleOutput($"[SERVER LOG] {player.Name} {message}");

            var nearestPlayers = NAPI.Player.GetPlayersInRadiusOfPlayer(3.0, player);

            foreach (Client client in nearestPlayers)
            {
                client.SendChatMessage($"{player.Name} (low) {message}");
            }
        }

        #endregion

        #region Player Messaging Commands (OOC)

        [Command("pm", Alias = "pm", GreedyArg = true)]
        public void CMD_PM(Client player, Client target, string message)
        {
            if (target.Equals(player))
            {
                //player.SendChatMessage("You can't message yourself.");
                //return;
            }

            target.SendChatMessage($"~y~PM from {player.Name}~w~: (( {message} )) ");
            player.SendChatMessage($"~y~PM to {target.Name}~w~: (( {message} ))");

            NAPI.Util.ConsoleOutput($"[PM LOG] {player.Name} said {message} to {target.Name} ((OOC))");
        }

        [Command("ooc", Alias = "b", GreedyArg = true)]
        public void CMD_OOC(Client player, string message)
        {
            NAPI.Util.ConsoleOutput($"[SERVER LOG] {player.Name} (( {message} )) ");

            var nearestPlayers = NAPI.Player.GetPlayersInRadiusOfPlayer(5.0, player);

            foreach (Client client in nearestPlayers)
            {
                client.SendChatMessage($"{player.Name} ~m~(( {message} )) ");
            }
        }

        #endregion

        #region Admin Commands

        [Command("freeze")]
        public void CMD_Freeze(Client player, Client target, bool freeze)
        {
            NAPI.ClientEvent.TriggerClientEvent(target, "FreezePlayerClient", freeze);
            string str = (freeze) ? "frozen" : "unfrozen";
            player.SendChatMessage($"You have {str} {target.Name}");
            target.SendChatMessage($"You were {str} by {player.Name}");
        }

        

        #endregion
    }
}
